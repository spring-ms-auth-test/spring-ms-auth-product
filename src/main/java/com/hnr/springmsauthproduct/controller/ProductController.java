package com.hnr.springmsauthproduct.controller;

import com.hnr.springmsauthproduct.client.UserClient;
import com.hnr.springmsauthproduct.model.Product;
import com.hnr.springmsauthproduct.model.ProductEntity;
import com.hnr.springmsauthproduct.model.ResponseModel;
import com.hnr.springmsauthproduct.model.UserEntity;
import com.hnr.springmsauthproduct.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("product")
public class ProductController extends ResponseBuilder {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserClient userClient;

    private static final String secret = "dhfw7y48t78493758vb38753v783485687487675nv7";

    //    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/all")
    public ResponseEntity<ResponseModel> getAllProducts() {
        List<Product> products = productRepository.findAll().stream().map(productEntity -> {
            Product product = new Product(productEntity);
            product.setUser(userClient.getUserById(productEntity.getUserId(), secret));
            System.out.println("Feign username: " + userClient.getUserPrincipalName());
            return product;
        }).collect(Collectors.toList());

        return makeSuccessResponse(products);
    }

    @GetMapping("/my-products")
    public ResponseEntity<ResponseModel> getMyProducts() {
        UserEntity currentUser = userClient.getCurrentUser().getData();
        if (currentUser == null || currentUser.getId() == 0) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        List<Product> products = productRepository.findByUserId(currentUser.getId()).stream().map(productEntity -> {
            Product product = new Product(productEntity);
            product.setUser(currentUser);
            return product;
        }).collect(Collectors.toList());

        return makeSuccessResponse(products);
    }

    @PostMapping
    public ResponseEntity<ResponseModel> add(@RequestBody Product product) {

        if (product == null) {
            return makeInvalidParamsResponse();
        }

        product.setId(0);

        UserEntity currentUser = userClient.getCurrentUser().getData();
        product.setUser(currentUser);
        ProductEntity productEntity = new ProductEntity(product);
        productRepository.save(productEntity);

        product.setId(productEntity.getId());
        return makeSuccessResponse(product);
    }

    @PutMapping
    public ResponseEntity<ResponseModel> update(@RequestBody Product product) {

        if (product == null || product.getId() < 1) {
            return makeInvalidParamsResponse();
        }

        UserEntity currentUser = userClient.getCurrentUser().getData();
        product.setUser(currentUser);
        ProductEntity productEntity = new ProductEntity(product);
        productRepository.saveAndFlush(productEntity);

        return makeSuccessResponse(product);
    }

    @DeleteMapping
    public ResponseEntity<ResponseModel> delete(@RequestParam long id) {

        if (id < 1) {
            return makeInvalidParamsResponse();
        }

        ProductEntity productEntity = productRepository.findById(id).orElse(null);
        if (productEntity == null) {
            return makeNotFoundResponse();
        }

        UserEntity currentUser = userClient.getCurrentUser().getData();

        if(productEntity.getUserId() != currentUser.getId()){
            return makeUnauthorizedResponse();
        }

        productRepository.delete(productEntity);
        return makeSuccessResponse(null);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseModel> getById(@PathVariable long id) {

        if (id < 1) {
            return makeInvalidParamsResponse();
        }

        ProductEntity productEntity = productRepository.findById(id).orElse(null);

        if (productEntity == null) {
            return makeNotFoundResponse();
        }

        Product product = new Product(productEntity);
        product.setUser(userClient.getUserById(productEntity.getUserId(), secret));
        return makeSuccessResponse(product);
    }

    @GetMapping("/by-user/{userId}")
    public ResponseEntity<ResponseModel> getByUserId(@PathVariable long userId) {

        if (userId < 1) {
            return makeInvalidParamsResponse();
        }

        List<Product> products = productRepository.findByUserId(userId).stream().map(productEntity -> {
            Product product = new Product(productEntity);
            product.setUser(userClient.getUserById(productEntity.getUserId(), secret));
            System.out.println("Feign username: " + userClient.getUserPrincipalName());
            return product;
        }).collect(Collectors.toList());

        return makeSuccessResponse(products);
    }

    @GetMapping("/home")
    public String getHome() {
        return "Product home page";
    }
}
