package com.hnr.springmsauthproduct.repository;

import com.hnr.springmsauthproduct.model.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

    List<ProductEntity> findByUserId(long userId);
}
