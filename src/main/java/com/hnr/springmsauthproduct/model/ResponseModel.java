package com.hnr.springmsauthproduct.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

@Data
@AllArgsConstructor
public class ResponseModel<T> implements Serializable {

    public static final int STATUS_SUCCESS = 1000;
    public static final int STATUS_SERVER_ERROR = 5000;
    public static final int STATUS_NOT_FOUND = 4000;
    public static final int STATUS_INVALID_PARAMS = 4100;
    public static final int STATUS_UNAUTHORIZED = 4100;

    public static final String MSG_SUCCESS = "Success";
    public static final String MSG_SERVER_ERROR = "Internal server error";
    public static final String MSG_NOT_FOUND = "Requested item not found";
    public static final String MSG_INVALID_PARAMS = "Invalid parameters";
    public static final String MSG_UNAUTHORIZED = "You are not authorized to perform this operation";

    private int code;
    private String msg;
    private T data;
    private HashMap<String, Object> metaData;
}
