package com.hnr.springmsauthproduct.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
public class Product implements Serializable {

    private long id;
    private UserEntity user;
    private String name;
    private int count;

    public Product(ProductEntity productEntity) {
        this.id = productEntity.getId();
        this.name = productEntity.getName();
        this.count = productEntity.getCount();
    }
}