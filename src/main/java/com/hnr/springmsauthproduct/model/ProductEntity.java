package com.hnr.springmsauthproduct.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "tbl_product")
public class ProductEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private long userId;

    private String name;
    private int count;

    public ProductEntity() {
    }

    public ProductEntity(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.count = product.getCount();
        UserEntity user = product.getUser();
        if (user != null) {
            this.userId = user.getId();
        }
    }
}