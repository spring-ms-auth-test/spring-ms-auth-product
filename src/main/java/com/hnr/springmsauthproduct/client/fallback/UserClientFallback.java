package com.hnr.springmsauthproduct.client.fallback;

import com.hnr.springmsauthproduct.client.UserClient;
import com.hnr.springmsauthproduct.model.ResponseModel;
import com.hnr.springmsauthproduct.model.UserEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class UserClientFallback implements UserClient {

    @Override
    public UserEntity getUserById(long id, String secret) {
        return new UserEntity(id, "Default user", "user");
    }

    @Override
    public String getUserPrincipalName() {
        return "Default name";
    }

    @Override
    public ResponseModel<UserEntity> getCurrentUser() {
        UserEntity userEntity = new UserEntity(0, "Default user", "user");
        return new ResponseModel<>(ResponseModel.STATUS_SERVER_ERROR, ResponseModel.MSG_SERVER_ERROR, userEntity, new HashMap<>()) ;
    }
}
