package com.hnr.springmsauthproduct.client;

import com.hnr.springmsauthproduct.client.fallback.UserClientFallback;
import com.hnr.springmsauthproduct.model.ResponseModel;
import com.hnr.springmsauthproduct.model.UserEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "auth-service", fallback = UserClientFallback.class)
public interface UserClient {

    @GetMapping("/user/user-by-id/{id}")
    UserEntity getUserById(@PathVariable long id, @RequestParam String secret);

    @GetMapping("/user/principal-name")
    String getUserPrincipalName();

    @GetMapping("/user/current")
    ResponseModel<UserEntity> getCurrentUser();
}
